//
// Created by david on 22/03/2022.
//

#ifndef TP2_SHAPE_HPP
#define TP2_SHAPE_HPP
#include "vector"
#include "Point.hpp"
#include "string"
class Shape {
public:
    explicit Shape(char point);
    std::vector<std::vector<Point>> getPointsOfShape();
protected:
    virtual void drawShape_(bool useBase);
    virtual bool detectPoint_(int x, int y);
    std::vector<std::vector<Point>> points_;
    char point_;
};


#endif //TP2_SHAPE_HPP
