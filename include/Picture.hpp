//
// Created by david on 15/03/2022.
//

#ifndef TP2_PICTURE_HPP
#define TP2_PICTURE_HPP
#define DEFAULT_DIMENSION 1000
#define MINIMUM_DIMENSION 0
#define BLANK ' '
#include "vector"
#include "Shape.hpp"
#include "fstream"
#include "iostream"
class Picture{
public:
    Picture();
    Picture(int max_h_dimension, int max_v_dimension);
    Picture(char fill);
    Picture(int max_h_dimension, int max_v_dimension,char fill);
    void clearCanvas();
    int registerToFile(const std::string& filename);
    int readFile(const std::string& inputFilename, const std::string& outputFilename);
    int registerShape(Shape &shape);
private:
    std::vector<std::string> getImageInfo_(std::vector<char> &image);
    int getNBOfBlackPixels_(std::vector<char> &image);
    int max_h_dimension_;
    int max_v_dimension_;
    int offset_v_dimension_;
    int offset_h_dimension_;
    char fill_;
    std::vector<std::vector<char>> canvas_;
};
#endif //TP2_PICTURE_HPP
