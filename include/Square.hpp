//
// Created by david on 15/03/2022.
//

#ifndef TP2_SQUARE_HPP
#define TP2_SQUARE_HPP

#include "Shape.hpp"

class Square: public Shape{
public:
    Square(char point, int side);
    Square& operator=(Square& other);
    Square(const Square &other);
private:
    void drawShape_(bool useBase) override;
    bool detectPoint_(int x,int y) override;
    int side_;
    std::string BASE_SQUARE_=
                             /*"########\n"
                             "#      #\n"
                             "#      #\n"
                             "#      #\n"
                             "#      #\n"
                             "#      #\n"
                             "########\n";*/
            "    \n"
            " ## \n"
            " ## \n"
            "    \n";
};
#endif //TP2_SQUARE_HPP
