//
// Created by david on 25/03/2022.
//

#ifndef TP2_PROGRAM_OPTIONS_HPP
#define TP2_PROGRAM_OPTIONS_HPP

class program_options {
public:
    void parse(int argc, char *argv[]);
    std::vector<std::string> getFiles();
    std::vector<std::string> input_files;
};

#endif //TP2_PROGRAM_OPTIONS_HPP
