//
// Created by david on 15/03/2022.
//

#ifndef TP2_CIRCLE_HPP
#define TP2_CIRCLE_HPP


#include "Shape.hpp"
class Circle : public Shape{
public:
    Circle(int radius, char point);
    Circle& operator=(Circle& other);
    Circle(const Circle &other);
private:
    int radius_;
    void drawShape_(bool useBase) override;
    bool detectPoint_(int x, int y) override;
    std::string BASE_CIRCLE_=
                            /* "     #####      \n"
                             "   ##     ##    \n"
                             "  #         #   \n"
                             "  #         #   \n"
                             "   ##     ##    \n"
                             "     #####      \n";*/
            "      \n"
            "  ##  \n"
            " #### \n"
            "  ##  \n"
            "      \n";
};


#endif //TP2_CIRCLE_HPP
