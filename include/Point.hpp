//
// Created by david on 15/03/2022.
//

#ifndef TP2_POINT_HPP
#define TP2_POINT_HPP
#define DEFAULT_POINT '#'
#define BLANK_POINT ' '
class Point{
public:
    Point();
    Point(char point);
    Point(Point *other);
    char getPoint();
    void setPoint(char point);
private:
    char point_;
};
#endif //TP2_POINT_HPP
