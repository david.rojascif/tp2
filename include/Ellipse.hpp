//
// Created by david on 15/03/2022.
//

#ifndef TP2_ELLIPSE_HPP
#define TP2_ELLIPSE_HPP

#include "Shape.hpp"
#include "math.h"
class Ellipse: public Shape {
public:
    Ellipse(char point, int f1, int f2);
    Ellipse& operator=(Ellipse& other);
    Ellipse(const Ellipse &other);
private:
    void drawShape_(bool useBase) override;
    bool detectPoint_(int x, int y) override;
    int f1_;
    int f2_;
    std::string BASE_ELLIPSE_=
                             /*"      ##       \n"
                             "    ##  ##     \n"
                             "   #      #    \n"
                             "  #        #   \n"
                             "   #      #    \n"
                             "    ##  ##     \n"
                             "      ##       \n";*/
            "    \n"
            " ## \n"
            "####\n"
            "####\n"
            " ## \n"
            "    \n";
};


#endif //TP2_ELLIPSE_HPP
