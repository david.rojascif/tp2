//
// Created by david on 15/03/2022.
//

#ifndef TP2_RECTANGLE_HPP
#define TP2_RECTANGLE_HPP


#include "Shape.hpp"

class Rectangle: public Shape {
public:
    Rectangle(int height, int length, char point);
    Rectangle& operator=(Rectangle& other);
    Rectangle(const Rectangle &other);
private:
    void drawShape_(bool useBase) override;
    bool detectPoint_(int x, int y) override;
    int height_;
    int length_;
    std::string BASE_RECTANGLE_=
                                /*"#####\n"
                                "#   #\n"
                                "#   #\n"
                                "#   #\n"
                                "#   #\n"
                                "#####\n";*/
            "    \n"
            " ## \n"
            " ## \n"
            " ## \n"
            "    \n";
};


#endif //TP2_RECTANGLE_HPP
