//
// Created by david on 15/03/2022.
//

#include "Point.hpp"

Point::Point() : point_(DEFAULT_POINT){}

Point::Point(char point) : point_(point){}

char Point::getPoint() {
    return point_;
}

void Point::setPoint(char point) {
    this->point_=point;
}

Point::Point(Point *other) {
    this->point_=other->point_;
}
