//
// Created by david on 15/03/2022.
//

#include "Square.hpp"

Square::Square(char point, int side) : Shape(point),side_(side) {
    Square::drawShape_(true);
}

void Square::drawShape_(bool useBase) {
    if (useBase){
        std::vector<Point> line;
        for (char c:BASE_SQUARE_) {
            Point point= new Point();
            if (c==BLANK_POINT){
                point.setPoint(BLANK_POINT);
                line.push_back(point);
            }else if (c=='\n'){
                point.setPoint('\n');
                line.push_back(point);
                points_.push_back(line);
                std::vector<Point> newLine;
                line=newLine;
            }else{
                point.setPoint(point_);
                line.push_back(point);
            }
        }
    }else{
        for (int i = 0; i < side_; ++i) {
            std::vector<Point> line;
            for (int j = 0; j < side_; ++j) {
                Point point= new Point();
                if (Square::detectPoint_(i, j)){
                    point.setPoint(point_);
                } else{
                    point.setPoint(BLANK_POINT);
                }
                line.push_back(point);
            }
            points_.push_back(line);
        }
    }
}

bool Square::detectPoint_(int x, int y) {
    return (x==0)||(y==0)||(x==side_)||(y==side_);
}

Square::Square(const Square &other) :
Shape(other.point_),side_(other.side_) {
    this->side_=other.side_;
    this->point_=other.point_;
    this->points_=other.points_;
    Square::drawShape_(true);
}

Square &Square::operator=(Square &other) {
    this->side_=other.side_;
    this->point_=other.point_;
    this->points_=other.points_;
    Square::drawShape_(true);
    return *this;
}

