//
// Created by david on 25/03/2022.
//

#include <string>
#include <vector>
#include "program_options.hpp"

void program_options::parse(int argc, char *argv[]) {
//source https://medium.com/@mostsignificant/3-ways-to-parse-command-line-arguments-in-c-quick-do-it-yourself-or-comprehensive-36913284460f
    const std::vector<std::string> args(argv + 1, argv + argc);
    for (const auto &arg: args) {
        if (arg == "-i" || arg == "-o")continue;
        input_files.push_back(arg);
    }
}

std::vector<std::string> program_options::getFiles() {
    return input_files;
}
