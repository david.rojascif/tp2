//
// Created by david on 15/03/2022.
//

#include "../include/Picture.hpp"

Picture::Picture(int max_h_dimension, int max_v_dimension):
        max_h_dimension_(max_h_dimension),
        max_v_dimension_(max_v_dimension),
        offset_v_dimension_(MINIMUM_DIMENSION),
        offset_h_dimension_(MINIMUM_DIMENSION),
        fill_(BLANK){
}

Picture::Picture():
        max_h_dimension_(DEFAULT_DIMENSION), max_v_dimension_(DEFAULT_DIMENSION),
        offset_v_dimension_(MINIMUM_DIMENSION), offset_h_dimension_(MINIMUM_DIMENSION),
        fill_(BLANK){
}

Picture::Picture(int max_h_dimension, int max_v_dimension,char fill):
        max_h_dimension_(max_h_dimension),
        max_v_dimension_(max_v_dimension),
        offset_v_dimension_(MINIMUM_DIMENSION),
        offset_h_dimension_(MINIMUM_DIMENSION),
        fill_(fill){
}

Picture::Picture(char fill):
        max_h_dimension_(DEFAULT_DIMENSION), max_v_dimension_(DEFAULT_DIMENSION),
        offset_v_dimension_(MINIMUM_DIMENSION), offset_h_dimension_(MINIMUM_DIMENSION),
        fill_(fill){
}

int Picture::registerToFile(const std::string& filename) {
    std::ofstream file(filename);
    if (!file){
        std::cout <<"Couldn't open or create file.\n";
        return 0;
    } else{
        for (std::vector<char> l:canvas_) {
            for (char c:l) {
                file << c;
                std::cout << c;
            }
        }
        file.close();
        return 1;
    }
}

int Picture::registerShape(Shape &shape) {
    std::vector<std::vector<Point>> shapeToRegister= shape.getPointsOfShape();

    int shapeY=shapeToRegister.size();
    int shapeX=shapeToRegister[0].size();
    if ( offset_v_dimension_+shapeY >  max_v_dimension_||
        offset_h_dimension_+shapeX > max_h_dimension_){
        std::cout <<"No more space, please clear canvas.\n";
        return 0;
    }
    for (std::vector<Point> p:shapeToRegister) {
        std::vector<char> line;
        for (Point point: p) {
            line.push_back(point.getPoint());
        }
        canvas_.push_back(line);
    }
    int v= int(shapeToRegister.size()) + offset_v_dimension_;
    int h=int(shapeToRegister[0].size()) + offset_v_dimension_;
    offset_v_dimension_+=v;
    offset_h_dimension_+=h;
    return 1;
}

void Picture::clearCanvas() {
    delete &(canvas_);
    canvas_=std::vector<std::vector<char>>();
    offset_v_dimension_=0;
    offset_h_dimension_=0;
}

int Picture::readFile(const std::string& inputFilename, const std::string& outputFilename) {
    std::ifstream inputFile(inputFilename);
    std::ofstream outputFile(outputFilename);
    if (!inputFile){
        std::cout <<"Couldn't open or create inputFile.\n";
        return 0;
    } else{
        std::vector<char> image;
        char cursor;
        while (1){
            cursor=inputFile.get();
            image.push_back(cursor);
            if (inputFile.eof()){
                break;
            }
        }
        if (!outputFile){
            std::cout <<"Couldn't open or create outputFile.\n";
            return 0;
        } else{
            std::vector<std::string> imageInfo=getImageInfo_(image);
            for(std::string line:imageInfo){
                for (char c: line) {
                    outputFile << c;
                    std::cout << c;
                }
            }
            outputFile.close();
        }
    }
    return 1;
}

std::vector<std::string> Picture::getImageInfo_(std::vector<char> &image) {
    std::vector<std::string> imageInfo;
    imageInfo.push_back("\nDimmensions= ");
    imageInfo.push_back(std::to_string(image.size()));
    imageInfo.push_back("X");
    imageInfo.push_back(std::to_string(image.size()));
    int nbOfBlackPixels=getNBOfBlackPixels_(image);
    imageInfo.push_back("\nNumber of black pixels= ");
    imageInfo.push_back(std::to_string(nbOfBlackPixels));
    int percentageOFBlackPixels= 100*nbOfBlackPixels/image.size();
    imageInfo.push_back("\nPercentage of black pixels= ");
    imageInfo.push_back(std::to_string(percentageOFBlackPixels));
    imageInfo.push_back("%");
    return imageInfo;
}

int Picture::getNBOfBlackPixels_(std::vector<char> &image) {
    int count=0;
    for (int i = 0; i < int(image.size()); ++i) {
        if (image[i]==BLANK)count++;
    }
    return image.size()-count;
}

