//
// Created by david on 15/03/2022.
//
#include "Picture.hpp"
#include "Circle.hpp"
#include "Ellipse.hpp"
#include "Square.hpp"
#include "Rectangle.hpp"
#include "random"
#include "program_options.hpp"
#include <string>
#include "chrono"
int main(int argc, char* argv[]){
    auto time = std::chrono::duration_cast<std::chrono::seconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
    program_options options;
    options.parse(argc,argv);
    std::vector<std::string> files= options.getFiles();
    int minSizeOfObjects=10;
    int maxSizeOfObjects=100;
    int maxShapesToDraw=50;
    std::default_random_engine randomEngine(time);
    std::uniform_int_distribution<int> distribution(minSizeOfObjects,maxSizeOfObjects);
    std::uniform_int_distribution<int> shapeDistribution(0,4);
    Picture picture;
    Circle circle= Circle(distribution(randomEngine), DEFAULT_POINT);
    Ellipse ellipse= Ellipse(DEFAULT_POINT,distribution(randomEngine),distribution(randomEngine));
    Square square= Square(DEFAULT_POINT,distribution(randomEngine));
    Rectangle rectangle= Rectangle(distribution(randomEngine),
                                   distribution(randomEngine),
                                   DEFAULT_POINT);
    for (int i = 0; i < maxShapesToDraw; ++i) {
        switch (shapeDistribution(randomEngine)) {
            case 1:
                if (!picture.registerShape(circle)){
                    Circle newCircle= Circle(distribution(randomEngine), DEFAULT_POINT);
                    circle=newCircle;
                }
                break;
            case 2:
                if (!picture.registerShape(ellipse)){
                    Ellipse newEllipse= Ellipse(DEFAULT_POINT,distribution(randomEngine),distribution(randomEngine));
                    ellipse=newEllipse;
                }
                break;
            case 3:
                if (!picture.registerShape(square)){
                    Square newSquare= Square(DEFAULT_POINT,distribution(randomEngine));
                    square=newSquare;
                }
                break;
            default:
                if (!picture.registerShape(rectangle)){
                    Rectangle newRectangle= Rectangle(distribution(randomEngine),
                                                      distribution(randomEngine),
                                                      DEFAULT_POINT);
                    rectangle=newRectangle;
                }
                break;
        }
    }
    std::string inputfile="../artOutput/"+files[0];
    picture.registerToFile(inputfile);

    std::string outputfile="../artOutput/"+files[1];
    picture.readFile(inputfile,outputfile);
    picture.clearCanvas();
    return EXIT_SUCCESS;
}
