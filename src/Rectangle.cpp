//
// Created by david on 15/03/2022.
//

#include "Rectangle.hpp"


Rectangle::Rectangle(int height, int length, char point):
Shape(point), height_(height), length_(length){
    Rectangle::drawShape_(true);
}

void Rectangle::drawShape_(bool useBase=true) {
    if (useBase){
        std::vector<Point> line;
        for (char c:BASE_RECTANGLE_) {
            Point point= new Point();
            if (c==BLANK_POINT){
                point.setPoint(BLANK_POINT);
                line.push_back(point);
            }else if (c=='\n'){
                point.setPoint('\n');
                line.push_back(point);
                points_.push_back(line);
                std::vector<Point> newLine;
                line=newLine;
            }else{
                point.setPoint(point_);
                line.push_back(point);
            }
        }
    } else{
        for (int i = 0; i < length_; ++i) {
            std::vector<Point> line;
            for (int j = 0; j < height_; ++j) {
                Point point= new Point();
                if (Rectangle::detectPoint_(i, j)){
                    point.setPoint(point_);
                }else{
                    point.setPoint(BLANK_POINT);
                }
                line.push_back(point);
            }
            points_.push_back(line);
        }
    }
}

bool Rectangle::detectPoint_(int x, int y) {
    return (x==0)||(y==0)||(x=length_)||(y==height_);
}

Rectangle &Rectangle::operator=(Rectangle &other) {
    this->height_=other.height_;
    this->length_= other.length_;
    this->point_=other.point_;
    this->points_=other.points_;
    Rectangle::drawShape_(true);
    return *this;
}

Rectangle::Rectangle(const Rectangle &other): Shape(other.point_) {
    this->height_=other.height_;
    this->length_= other.length_;
    this->point_=other.point_;
    this->points_=other.points_;
    Rectangle::drawShape_(true);
}
