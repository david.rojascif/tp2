//
// Created by david on 15/03/2022.
//

#include "Ellipse.hpp"

Ellipse::Ellipse(char point, int f1, int f2) : Shape(point),f1_(f1),f2_(f2){
    Ellipse::drawShape_(true);
}

void Ellipse::drawShape_(bool useBase=true) {
    if (useBase){
        std::vector<Point> line;
        for (char c:BASE_ELLIPSE_) {
            Point point= new Point();
            if (c==BLANK_POINT){
                point.setPoint(BLANK_POINT);
                line.push_back(point);
            }else if (c=='\n'){
                point.setPoint('\n');
                line.push_back(point);
                points_.push_back(line);
                std::vector<Point> newLine;
                line=newLine;
            }else{
                point.setPoint(point_);
                line.push_back(point);
            }
        }
    }else{
        int v=(2* sqrt(f1_));
        int h=(2* sqrt(f2_));
        for (int i = 0; i < v; ++i) {
            std::vector<Point> line;
            for (int j = 0; j < h; ++j) {
                Point point= new Point();
                if (Ellipse::detectPoint_(i, j)){
                    point.setPoint(point_);
                } else{
                    point.setPoint(BLANK_POINT);
                }
                line.push_back(point);
            }
            points_.push_back(line);
        }
    }
}

bool Ellipse::detectPoint_(int x, int y) {
    return ((x-sqrt(f1_))*(x-sqrt(f1_))/f1_)+((x-sqrt(f2_))*(x-sqrt(f2_))/f2_)==1;
}

Ellipse &Ellipse::operator=(Ellipse &other) {
    this->f1_=other.f1_;
    this->f2_=other.f2_;
    this->points_=other.points_;
    this->point_=other.point_;
    this->Ellipse::drawShape_(true);
    return *this;
}

Ellipse::Ellipse(const Ellipse &other) :
Shape(other.point_),
f1_(other.f1_),
f2_(other.f2_){
    this->f1_=other.f1_;
    this->f2_=other.f2_;
    this->points_=other.points_;
    this->point_=other.point_;
    this->Ellipse::drawShape_(true);
}
