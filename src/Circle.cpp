//
// Created by david on 15/03/2022.
//

#include "Circle.hpp"

Circle::Circle(int radius, char point) : Shape(point), radius_(radius) {
    Circle::drawShape_(true);
}

void Circle::drawShape_(bool useBase=true) {
    if (useBase){
        std::vector<Point> line;
        for (char c:BASE_CIRCLE_) {
            Point point= new Point();
            if (c==BLANK_POINT){
                point.setPoint(BLANK_POINT);
                line.push_back(point);
            }else if (c=='\n'){
                point.setPoint('\n');
                line.push_back(point);
                points_.push_back(line);
                std::vector<Point> newLine;
                line=newLine;
            }else{
                point.setPoint(point_);
                line.push_back(point);
            }
        }
    } else{
        for (int i = 0; i < radius_; ++i) {
            std::vector<Point> line;
            for (int j = 0; j < radius_; ++j) {
                Point point= new Point();
                if (Circle::detectPoint_(i, j)){
                    point.setPoint(point_);
                }else{
                    point.setPoint(BLANK_POINT);
                }
                line.push_back(point);
            }
            points_.push_back(line);
        }
    }
}

Circle &Circle::operator=(Circle &other) {
    this->points_=other.points_;
    this->radius_=other.radius_;
    this->point_=other.point_;
    this->Circle::drawShape_(true);
    return *this;
}

Circle::Circle(const Circle &other) : Shape(other.point_) {
    this->points_=other.points_;
    this->radius_=other.radius_;
    this->point_=other.point_;
    this->Circle::drawShape_(true);
}

bool Circle::detectPoint_(int x, int y) {
    return (x-radius_)*(x-radius_)+(y-radius_)*(y-radius_)==radius_*radius_;
}
